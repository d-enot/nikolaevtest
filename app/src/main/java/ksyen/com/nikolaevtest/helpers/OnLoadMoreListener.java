package ksyen.com.nikolaevtest.helpers;

public interface OnLoadMoreListener {

    void onLoadMore();

}
