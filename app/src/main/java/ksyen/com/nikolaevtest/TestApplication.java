package ksyen.com.nikolaevtest;

import android.app.Application;

import com.activeandroid.ActiveAndroid;

public class TestApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
    }
}
