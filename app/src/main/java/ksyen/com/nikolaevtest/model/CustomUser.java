package ksyen.com.nikolaevtest.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.util.SQLiteUtils;

import java.util.List;

@Table(name = "users")
public class CustomUser extends Model {

    @Column(name = "unique_id", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public int uniqueId;
    @Column(name = "first_name")
    public String firstName;
    @Column(name = "last_name")
    public String lastName;

    public CustomUser() {
        super();
    }

    public static List<CustomUser> getUsers(int limit, int offset) {
        return SQLiteUtils.rawQuery(CustomUser.class,
                "SELECT id, unique_id, first_name, last_name from users LIMIT "
                        + "'" + limit + "'" + " OFFSET " + "'" + offset + "'", null);
    }
}

