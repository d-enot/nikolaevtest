package ksyen.com.nikolaevtest.ui.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import ksyen.com.nikolaevtest.helpers.OnLoadMoreListener;
import ksyen.com.nikolaevtest.R;
import ksyen.com.nikolaevtest.model.CustomUser;
import ksyen.com.nikolaevtest.ui.adapter.UsersAdapter;
import ksyen.com.nikolaevtest.ui.fragment.SetThresholdDialog;

public class MainActivity extends AppCompatActivity {

    public static final String PREFERENCES = "ksyen.com.nikolaevtest_prefferences";
    public static final String CURRENT_THRESHOLD = "current_threshold";
    static final int DEFAULT_MAX_ELEMENTS_IN_MEMORY = 50;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.list)
    RecyclerView mList;
    @BindString(R.string.main_activity_tittle)
    String mTitle;

    private List<CustomUser> mUsers;
    private UsersAdapter mAdapter;
    private int mThreshold;
    private int totalItemCount, lastVisibleItem;
    private boolean isLoading;
    private OnLoadMoreListener mOnLoadMoreListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        initToolbar();
        int savedThreshold = getSaveThreshold();
        if (savedThreshold == 0) {
            mThreshold = DEFAULT_MAX_ELEMENTS_IN_MEMORY;
        } else mThreshold = savedThreshold;
        fillUsersTable();
        initList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                openDialog(mThreshold);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private int getSaveThreshold() {
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES, MODE_PRIVATE);
        return sharedPreferences.getInt(CURRENT_THRESHOLD, 0);
    }

    private void openDialog(int currentData) {
        SetThresholdDialog dialog = SetThresholdDialog.newInstance(threshold -> {
            if (threshold > 0) {
                mThreshold = threshold;
            } else mThreshold = DEFAULT_MAX_ELEMENTS_IN_MEMORY;
            saveThreshold();
        }, currentData);
        dialog.show(getSupportFragmentManager(), "threshold_dialog");
    }

    private void saveThreshold() {
        SharedPreferences pref = getSharedPreferences(PREFERENCES, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(CURRENT_THRESHOLD, mThreshold);
        editor.commit();
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(mTitle);
    }

    private void fillUsersTable() {
        for(int i=0; i<4; i++){
            CustomUser user = new CustomUser();
            user.uniqueId = i;
            user.firstName = "first_name" + i;
            user.lastName = "last_name" + i;
            user.save();
        }
    }

    private void initList() {
        mUsers = new ArrayList<>();
        mUsers.addAll(CustomUser.getUsers(mThreshold, mUsers.size()));

        initOnMoreLoad();

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mList.setLayoutManager(linearLayoutManager);
        mAdapter = new UsersAdapter(mUsers, this);
        mList.setAdapter(mAdapter);

        mList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + mThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    private void initOnMoreLoad() {
        mOnLoadMoreListener = () -> {
            mUsers.add(null);
            mAdapter.notifyItemInserted(mUsers.size() - 1);

            new Handler().postDelayed(() -> {
                mUsers.remove(mUsers.size() - 1);
                mAdapter.notifyItemRemoved(mUsers.size());

                List<CustomUser> nextUsers = CustomUser.getUsers(mThreshold, mUsers.size());
                if (nextUsers != null&&nextUsers.size()>0) {
                    mUsers.addAll(nextUsers);
                    mAdapter.notifyDataSetChanged();
                }
                isLoading = false;
            }, 5000);
        };
    }
}
