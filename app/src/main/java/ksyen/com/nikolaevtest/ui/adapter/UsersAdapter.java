package ksyen.com.nikolaevtest.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ksyen.com.nikolaevtest.R;
import ksyen.com.nikolaevtest.model.CustomUser;

public class UsersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public static final int VIEW_TYPE_USER = 0;
    public static final int VIEW_TYPE_LOAD = 1;

    private List<CustomUser> mItems;
    private Context mContext;

    public UsersAdapter(List<CustomUser> mItems, Context mContext) {
        this.mItems = mItems;
        this.mContext = mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_USER) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_list, parent, false);
            return new UserHolder(view);
        } else if (viewType == VIEW_TYPE_LOAD) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_load_list, parent, false);
            return new LoadHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof UserHolder) {
            CustomUser user = mItems.get(position);
            UserHolder userViewHolder = (UserHolder) holder;
            userViewHolder.firstName.setText(user.firstName);
            userViewHolder.lastName.setText(user.lastName);
        } else if (holder instanceof LoadHolder) {
            LoadHolder loadingViewHolder = (LoadHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mItems.get(position) == null ? VIEW_TYPE_LOAD : VIEW_TYPE_USER;
    }

    static class LoadHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.progress_bar)
        ProgressBar progressBar;

        public LoadHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    static class UserHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.first_name)
        TextView firstName;
        @BindView(R.id.last_name)
        TextView lastName;

        public UserHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
