package ksyen.com.nikolaevtest.ui.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import ksyen.com.nikolaevtest.R;

public class SetThresholdDialog extends DialogFragment{

    public static final String CURRENT_THRESHOLD = "current_threshold";

    private CallBack callBack;

    public static SetThresholdDialog newInstance(CallBack callBack, int currentThreshold) {

        Bundle args = new Bundle();
        args.putInt(CURRENT_THRESHOLD, currentThreshold);

        SetThresholdDialog fragment = new SetThresholdDialog();
        fragment.setArguments(args);
        fragment.callBack = callBack;
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle args = getArguments();
        int currentThreshold = args.getInt(CURRENT_THRESHOLD);

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.threshold_dialog, null);
        TextView currentTxt = (TextView) v.findViewById(R.id.current_data);
        currentTxt.setText(String.valueOf(currentThreshold));
        final EditText newThresholdTxt = (EditText) v.findViewById(R.id.new_data);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(v);
        builder.setMessage(getActivity().getResources().getString(R.string.dialog_message));
        builder.setNegativeButton("Cancel",
                (dialogInterface, i) -> SetThresholdDialog.this.dismiss());
        builder.setPositiveButton("OK", (dialog, which) -> {
            String newData = newThresholdTxt.getText().toString();
            if (newData.isEmpty()) {
                callBack.onChanged(0);
            }else callBack.onChanged(Integer.parseInt(newData));
            SetThresholdDialog.this.dismiss();
        });

        return builder.create();
    }

    public interface CallBack {
        void onChanged(int threshold);
    }
}
